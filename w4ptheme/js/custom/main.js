/**
 * @file
 * Contains js for the site.
 */
/**  Global Media matches
/*   usage */
    //if (window.media.medium.matches) {
    //
    //}
window.media = {
    xsmall: window.matchMedia("only screen and (max-width: 480px)"),
    small: window.matchMedia("only screen and (max-width: 640px)"),
    medium: window.matchMedia("only screen and (max-width: 1024px)"),
    large: window.matchMedia("only screen and (max-width: 1440px)"),
    xlarge: window.matchMedia("only screen and (max-width: 1920px)")
};

(function ($) {

    $(function () {

    });

})(jQuery);